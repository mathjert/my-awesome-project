const express = require('express');
const app = express();
const PORT = 8080;
const path = require('path');

app
    .get('/', (req, res) => {
        console.log('awsm');
        res.sendFile(path.join(__dirname + '/source/index.html'));
    })
    .get('/game', (req, res) => {
        console.log('more awsm');
        res.sendFile(path.join(__dirname + '/source/game.html'));
    })
    .use('/scripts', express.static(path.join(__dirname + '/source/scripts'))).listen(PORT, () => console.log(`Listening on port ${PORT}`)); 

